
//#!/usr/bin/env node
const program = require('commander');

const { addContact, getContact } = require('./logic');

program
  .version('0.1.0')
  .description('Contact management system');

  program
    .command('programmer')
    .alias('-p')
    .description('ดูข้อมูลโปรแกรมเมอร์')
    .action(() => {
      console.log(`programmer คือ วิชิตะ`);
    });
    program
    .command('addContact <firstame> <lastname> <phone> <email>')
    .alias('a')
    .description('Add a contact')
    .action((firstname, lastname, phone, email) => {

      addContact({firstname, lastname, phone, email});
  });
    // program.parse(process.argv);
    console.log(process.argv);
